<?php

declare(strict_types=1);

namespace Drupal\config_help\FormElement;

use Drupal\Component\Utility\Html;
use Drupal\config_translation\FormElement\Textarea;
use Drupal\Core\Language\LanguageInterface;

/**
 * Defines the element for editing help topic body chunk translations.
 *
 * This class is associated with the help topic in the config schema.
 *
 * @see config_help.schema.yml
 */
class HelpTopicBodyChunk extends Textarea {

  /**
   * {@inheritdoc}
   */
  protected function getSourceElement(LanguageInterface $source_language, $source_config) {
    return parent::getSourceElement($source_language, Html::escape($source_config));
  }

}
