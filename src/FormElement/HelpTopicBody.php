<?php

declare(strict_types=1);

namespace Drupal\config_help\FormElement;

use Drupal\config_help\Entity\HelpTopic;
use Drupal\config_translation\FormElement\ListElement;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\TypedData\DataDefinitionInterface;

/**
 * Defines the element for editing help topic body translations.
 *
 * This class is associated with the help topic in the config schema.
 *
 * @see config_help.schema.yml
 */
class HelpTopicBody extends ListElement {

  /**
   * {@inheritdoc}
   */
  public function getTranslationBuild(LanguageInterface $source_language, LanguageInterface $translation_language, $source_config, $translation_config, array $parents, $base_key = NULL) {
    $build = [];

    $build['explanation'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Translate the inner text in each chunk of the body separately. Full body is also provided for reference.'),
    ];

    $build['rendered_body'] = [
      '#type' => 'details',
      '#title' => $this->t('Full body'),
      '#open' => FALSE,
    ];
    $build['rendered_body']['body'] = [
      '#type' => 'markup',
      '#markup' => $this->joinBody($source_config),
    ];

    $build += parent::getTranslationBuild($source_language, $translation_language, $source_config, $translation_config, $parents, $base_key);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function getGroupTitle(DataDefinitionInterface $definition, array $group_build) {
    return $this->t('Body chunk');
  }

  /**
   * Joins raw body chunks into HTML.
   *
   * @param array $raw_body
   *   Raw chunked body array.
   *
   * @return string
   *   String with body chunks joined.
   */
  protected function joinBody($raw_body) {
    $entity = HelpTopic::create(['body' => $raw_body]);
    return $entity->getBody();
  }

}
