<?php

declare(strict_types=1);

namespace Drupal\config_help\Plugin\Deriver;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\config_help\Plugin\HelpTopic\DerivedHelpTopicPlugin;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides derivative help topic plugins for help topic entities.
 */
class HelpTopicDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The help topic entity storage.
   */
  protected ConfigEntityStorageInterface $helpStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    $instance = new self();
    $instance->helpStorage = $container->get('entity_type.manager')->getStorage('config_help');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];
    foreach ($this->helpStorage->loadMultiple() as $entity_id => $entity) {
      /** @var \Drupal\help\HelpTopicPluginInterface $entity */
      $this->derivatives[$entity_id] = $this->fixUpDefinition($entity->getPluginDefinition(), $base_plugin_definition);
    }
    return $this->derivatives;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinition($derivative_id, $base_plugin_definition) {
    $entity = $this->helpStorage->load($derivative_id);
    return $this->fixUpDefinition($entity->getPluginDefinition(), $base_plugin_definition);
  }

  /**
   * Fixes up the plugin definition.
   *
   * @param array $definition
   *   The plugin definition to fix up.
   * @param array $base_plugin_definition
   *   The base plugin definition.
   *
   * @return array
   *   The plugin definition with a few additions.
   */
  protected function fixUpDefinition($definition, $base_plugin_definition) {
    $prefix = $base_plugin_definition['id'];
    $entity_id = $definition['id'];
    $plugin_id = $prefix . ':' . $entity_id;

    $definition['plugin_id'] = $plugin_id;
    $definition['id'] = $plugin_id;
    $definition['class'] = DerivedHelpTopicPlugin::class;
    return $definition;
  }

}
