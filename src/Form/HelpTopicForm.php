<?php

declare(strict_types=1);

namespace Drupal\config_help\Form;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;
use Drupal\config_help\Entity\HelpTopicInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\help\HelpTopicPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for editing a help topic.
 */
class HelpTopicForm extends EntityForm {

  /**
   * The help topic entity storage.
   */
  protected ConfigEntityStorageInterface $helpStorage;

  /**
   * The theme handler.
   */
  protected ThemeHandlerInterface $themeHandler;

  /**
   * The help topic plugin manager.
   */
  protected HelpTopicPluginManagerInterface $pluginManager;

  /**
   * The current user.
   */
  protected AccountInterface $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->helpStorage = $container->get('entity_type.manager')->getStorage('config_help');
    $instance->moduleHandler = $container->get('module_handler');
    $instance->themeHandler = $container->get('theme_handler');
    $instance->messenger = $container->get('messenger');
    $instance->stringTranslation = $container->get('string_translation');
    $instance->pluginManager = $container->get('plugin.manager.help_topic');
    $instance->currentUser = $container->get('current_user');
    return $instance;
  }

  /**
   * Checks for an existing help topic.
   *
   * @param string $id
   *   The entity ID.
   *
   * @return bool
   *   TRUE if this topic already exists, FALSE otherwise.
   */
  public function exists($id) {
    // Use load() method to leverage entity cache.
    return (bool) $this->helpStorage->load($id);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\config_help\Entity\HelpTopicInterface $entity */
    $entity = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#maxlength' => 100,
      '#default_value' => $entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'source' => ['label'],
        'error' => $this->t('The machine-readable name must be unique, and can only contain lowercase letters, numbers, and underscores.'),
      ],
    ];

    $form['langcode'] = [
      '#type' => 'language_select',
      '#default_value' => $entity->language()->getId(),
      '#title' => $this->t('Language'),
      '#languages' => LanguageInterface::STATE_ALL,
    ];

    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#default_value' => $entity->getBody(),
      '#format' => $this->getTextFormat($entity),
      '#description' => $this->t('You can use tokens like [route:url:ROUTE_NAME] and [help_topic:url:MACHINE_NAME] to insert URLs to administrative page routes and to other help topics into the text. Tokens can also be entered using | instead of :, to get around XSS filtering problems in visual editor forms.'),
    ];

    $form['relationships'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Relationships and hierarchy'),
    ];

    $form['relationships']['top_level'] = [
      '#type' => 'checkbox',
      '#default_value' => $entity->isTopLevel(),
      '#title' => $this->t('Top-level topic'),
      '#description' => $this->t('Check box if this topic should be displayed on the Help page topics list'),
    ];

    $form['relationships']['related'] = [
      '#title' => $this->t('Related topics'),
      '#description' => $this->t("Topics related to this topic. Comma-separated list of machine names."),
      '#type' => 'textfield',
      '#maxlength' => 5000,
      '#default_value' => Tags::implode($entity->getRelated()),
      '#autocomplete_route_name' => 'config_help.topic_autocomplete',
    ];

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    $state = clone $form_state;
    $state->setValue('top_level', (bool) $form_state->getValue('top_level'));
    $related = $form_state->getValue('related') ?? '';
    $state->setValue('related', Tags::explode($related));
    $body = $form_state->getValue('body');
    // As using setBody() on topic later.
    $state->unsetValue('body');
    $state->cleanValues();
    $state->setValue('body_format', $body['format']);
    // Prevent leading and trailing spaces in topic names.
    $state->setValue('label', trim($form_state->getValue('label')));

    /** @var \Drupal\config_help\Entity\HelpTopicInterface $topic */
    $topic = parent::buildEntity($form, $state);
    // Split body's HTML into chunks.
    return $topic->setBody($body['value']);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Make sure that the HTML in the body can at least be loaded/parsed.
    if (!Html::load($form_state->getValue('body')['value'])) {
      $form_state->setErrorByName('body', $this->t('Body HTML is malformed'));
    }

    // Make sure that the reference field only contains machine names of
    // actual help topics.
    $list = Tags::explode($form_state->getValue('related') ?? '');
    $existing = array_keys($this->pluginManager->getDefinitions());
    $missing = array_diff($list, $existing);
    if ($missing) {
      $form_state->setErrorByName('related', $this->t('Must be a comma-separated list of existing topic machine names (%problem)', [
        '%problem' => Tags::implode($missing),
      ]));
    }

    if ($form_state->hasValue('modules')) {
      $list = Tags::explode($form_state->getValue('modules'));
      $missing = [];
      foreach ($list as $module) {
        if (!$this->moduleHandler->moduleExists($module)) {
          $missing[] = $module;
        }
      }
      if ($missing) {
        $form_state->setErrorByName('modules', $this->t('Must be a comma-separated list of installed module machine names (%problem)', ['%problem' => Tags::implode($missing)]));
      }
    }

    if ($form_state->hasValue('themes')) {
      $list = Tags::explode($form_state->getValue('themes'));
      $missing = [];
      foreach ($list as $theme) {
        if (!$this->themeHandler->themeExists($theme)) {
          $missing[] = $theme;
        }
      }
      if ($missing) {
        $form_state->setErrorByName('themes', $this->t('Must be a comma-separated list of installed theme machine names (%problem)', ['%problem' => Tags::implode($missing)]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\config_help\Entity\HelpTopicInterface $entity */
    $entity = $this->entity;
    $status = $entity->save();

    // Redirect to the View page, unless the user cannot view the page.
    // Query parameter 'destination' will override this.
    if ($entity->access('view')) {
      $form_state->setRedirectUrl($entity->getViewUrl());
    }
    else {
      $form_state->setRedirect('entity.help_topic.collection');
    }

    $link = $entity->getViewLink()->toString();
    $ops_link = $entity->getViewLink($this->t('View'))->toString();
    $args = ['@link' => $link];
    if ($status == SAVED_UPDATED) {
      $message = $this->t('The help topic @link has been updated.', $args);
      $this->messenger->addMessage($message);
      $this->getLogger('config_help')->notice('The help topic @link has been updated.', $args + [
        'link' => $ops_link,
      ]);
    }
    else {
      $message = $this->t('The help topic @link has been added.', $args);
      $this->messenger->addMessage($message);
      $this->getLogger('config_help')->notice('The help topic @link has been added.', $args + [
        'link' => $ops_link,
      ]);
    }
    return $status;
  }

  /**
   * Get an appropriate default filter format.
   *
   * @param \Drupal\config_help\Entity\HelpTopicInterface $entity
   *   The help topic entity.
   *
   * @return string|null
   *   The machine name of either the currently-set format, or of an appropriate
   *   default, or null if there isn't a reasonable default.
   */
  protected function getTextFormat(HelpTopicInterface $entity) {
    if ($entity->getBodyFormat()) {
      return $entity->getBodyFormat();
    }

    // See if a format called 'help' exists and is accessible by the current
    // user.
    $formats = filter_formats($this->currentUser);
    if (isset($formats['help'])) {
      return 'help';
    }

    // If not, just return NULL.
    return NULL;
  }

}
