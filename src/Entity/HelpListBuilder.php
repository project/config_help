<?php

declare(strict_types=1);

namespace Drupal\config_help\Entity;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides an entity list builder class for Help topic entities.
 */
class HelpListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'label' => $this->t('Title'),
      'id' => $this->t('Machine name'),
      'top_level' => [
        'data' => $this->t('Top level'),
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\config_help\Entity\HelpTopicInterface $entity */
    $row = [];

    $row['label']['data'] = [
      '#type' => 'link',
      '#title' => $entity->label(),
      '#url' => $entity->getViewUrl(),
    ];

    $row['id'] = $entity->id();

    $row['top_level'] = ($entity->isTopLevel()) ? $this->t('Yes') : $this->t('No');

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    // Override the default method to sort by label, since the paged table
    // is eventually sorted by label within each page.
    $query = $this->getStorage()->getQuery()
      ->sort($this->entityType->getKey('label'));

    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    // The default operations from the parent are edit and delete. Add
    // view.
    if ($entity->access('view')) {
      $operations['view'] = [
        'title' => $this->t('View'),
        'weight' => 20,
        'url' => $entity->getViewUrl(),
      ];
    }
    return $operations;
  }

}
