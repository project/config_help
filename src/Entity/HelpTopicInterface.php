<?php

declare(strict_types=1);

namespace Drupal\config_help\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines an interface for a help topic entity.
 *
 * @see \Drupal\config_help\Entity\HelpTopic
 */
interface HelpTopicInterface extends ConfigEntityInterface {

  /**
   * Returns the body of the topic.
   *
   * @return string
   *   The HTML-formatted body of the topic.
   */
  public function getBody();

  /**
   * Sets the body value for the topic.
   *
   * @param string $body
   *   The HTML-formatted body to save in the topic.
   *
   * @return $this
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   *   If the HTML is malformed.
   */
  public function setBody($body);

  /**
   * Returns text format for the body of the topic.
   *
   * @return string
   *   The machine name of the text format for the topic body.
   */
  public function getBodyFormat();

  /**
   * Sets the text format for the body of the topic.
   *
   * @param string $format
   *   The machine name of the text format to use for the topic body.
   *
   * @return $this
   */
  public function setBodyFormat($format);

  /**
   * Returns whether this is a top-level topic or not.
   *
   * @return bool
   *   TRUE if this is a topic that should be displayed on the Help topics
   *   list; FALSE if not.
   */
  public function isTopLevel();

  /**
   * Sets whether this is a top-level topic or not.
   *
   * @param bool $top_level
   *   TRUE if this is a topic that should be displayed on the Help topics
   *   list; FALSE if not.
   *
   * @return $this
   */
  public function setTopLevel($top_level);

  /**
   * Returns the IDs of related topics.
   *
   * @return string[]
   *   Array of the plugin IDs of related topics.
   */
  public function getRelated();

  /**
   * Sets the related topics.
   *
   * @param string[] $topics
   *   Array of related topic plugin IDs.
   *
   * @return $this
   */
  public function setRelated(array $topics);

  /**
   * Returns the ID to use for this entity when wrapped in a plugin.
   *
   * @return string
   *   Plugin ID for this entity.
   */
  public function getPluginId();

  /**
   * Returns the entity information in the form of a plugin definition.
   *
   * @return array
   *   Array of entity information in the format of a plugin definition.
   */
  public function getPluginDefinition();

  /**
   * Returns the URL for viewing the help topic.
   *
   * @param array $options
   *   (optional) See
   *   \Drupal\Core\Routing\UrlGeneratorInterface::generateFromRoute() for the
   *    available options.
   *
   * @return \Drupal\Core\Url
   *   A URL object containing the URL for viewing the help topic.
   */
  public function getViewUrl(array $options = []);

  /**
   * Returns a link for viewing the help topic.
   *
   * @param string|null $text
   *   (optional) Link text to use for the link. If NULL, defaults to the
   *   topic title.
   * @param array $options
   *   (optional) See
   *   \Drupal\Core\Routing\UrlGeneratorInterface::generateFromRoute() for the
   *    available options.
   *
   * @return \Drupal\Core\Link
   *   A link object for viewing the topic.
   */
  public function getViewLink($text = NULL, array $options = []);

}
