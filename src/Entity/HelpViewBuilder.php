<?php

declare(strict_types=1);

namespace Drupal\config_help\Entity;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Utility\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a help topic view builder.
 */
class HelpViewBuilder extends EntityViewBuilder {

  /**
   * The token replacement service class.
   */
  protected Token $token;

  /**
   * The help topic storage instance.
   */
  protected ConfigEntityStorageInterface $helpStorage;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->token = $container->get('token');
    $instance->helpStorage = $container->get('entity_type.manager')->getStorage('config_help');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function viewMultiple(array $entities = [], $view_mode = 'full', $langcode = NULL) {
    $output = [];

    /** @var \Drupal\config_help\Entity\HelpTopicInterface[] $entities */
    foreach ($entities as $entity_id => $help_topic) {
      // Get the cache information and other build defaults.
      $build = $this->getBuildDefaults($help_topic, $view_mode);
      $build['#langcode'] = $langcode;
      $build['#title'] = $help_topic->label();

      // Preprocess the body for tokens entered with | instead of :.
      // See issue https://www.drupal.org/project/2369943/issues/2943974.
      $body = $help_topic->getBody();
      $matches = [];
      // Find things that look like tokens, but using | instead of :, for
      // example [foo|bar_baz|bang]. Allowable characters for the token pieces
      // are letters, numbers, underscores, plus . and - characters.
      if (preg_match_all('/\[([\w.\-]+\|)+[\w.\-]+\]/', $body, $matches)) {
        // Replace the | with : in each token that was found.
        $tokens = array_unique($matches[0]);
        $replacements = [];
        foreach ($tokens as $token) {
          $replacements[] = str_replace('|', ':', $token);
        }
        // Replace all the tokens in the string. This could have all been done
        // with one preg_replace instead of this whole if() statement, but the
        // code would have been very difficult to read.
        $body = str_replace($tokens, $replacements, $body);
      }

      // Add in the body, with token replacement.
      $bubbleable_metadata = new BubbleableMetadata();
      $build['#body'] = [
        '#type' => 'processed_text',
        '#text' => $this->token->replace($body, [], [], $bubbleable_metadata),
        '#format' => $help_topic->getBodyFormat(),
      ];
      $bubbleable_metadata->applyTo($build['#body']);

      $output[$entity_id] = $build;
    }

    return $output;
  }

}
