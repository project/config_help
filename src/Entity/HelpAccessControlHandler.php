<?php

declare(strict_types=1);

namespace Drupal\config_help\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Determines entity access for Help topic entities.
 */
class HelpAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\config_help\Entity\HelpTopicInterface $entity */
    // For view, check the view permission.
    if ($operation == 'view') {
      return AccessResult::allowedIfHasPermission($account, 'access administration pages');
    }

    // For all remaining operations, use the generic administer permission.
    // Note that we've already checked something on the entity, so make sure to
    // add cache dependency.
    return AccessResult::allowedIfHasPermission($account, 'administer config help')->addCacheableDependency($entity);
  }

}
