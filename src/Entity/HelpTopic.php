<?php

declare(strict_types=1);

namespace Drupal\config_help\Entity;

use Drupal\config_help\HtmlChunker;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigValueException;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\help\HelpTopicPluginManagerInterface;

/**
 * Defines a configuration entity for help topics.
 *
 * @ConfigEntityType(
 *   id = "config_help",
 *   label = @Translation("Configurable help topic"),
 *   label_collection = @Translation("Configurable help topics"),
 *   config_prefix = "topic",
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\config_help\Form\HelpTopicForm",
 *       "edit" = "Drupal\config_help\Form\HelpTopicForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\config_help\Entity\HelpListBuilder",
 *     "view_builder" = "Drupal\config_help\Entity\HelpViewBuilder",
 *     "access" = "Drupal\config_help\Entity\HelpAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "top_level",
 *     "related",
 *     "body",
 *     "body_format",
 *   },
 *   admin_permission = "administer config help",
 *   links = {
 *     "collection" = "/admin/config/development/config-help",
 *     "add-form" = "/admin/config/development/config-help/add",
 *     "edit-form" = "/admin/config/development/config-help/manage/{config_help}",
 *     "delete-form" = "/admin/config/development/config-help/manage/{config_help}/delete",
 *   }
 * )
 */
class HelpTopic extends ConfigEntityBase implements HelpTopicInterface {

  /**
   * The topic machine name.
   */
  protected ?string $id;

  /**
   * The topic title.
   */
  protected ?string $label;

  /**
   * The unprocessed topic body text (in the configuration schema format).
   *
   * @var string[]
   */
  protected array $body = [];

  /**
   * The machine name of the text format for the body in HTML form.
   */
  protected string $body_format = '';

  /**
   * Whether or not the topic should appear on the help topics list.
   */
  protected bool $top_level = FALSE;

  /**
   * List of related topic machine names.
   *
   * @var string[]
   */
  protected ?array $related = [];

  /**
   * The help topic plugin manager.
   */
  protected HelpTopicPluginManagerInterface $pluginManager;

  /**
   * {@inheritdoc}
   */
  public function getBody() {
    return HtmlChunker::joinChunks($this->get('body'));
  }

  /**
   * {@inheritdoc}
   */
  public function setBody($body) {
    $chunks = HtmlChunker::chunkHtml($body);
    if ($chunks === FALSE) {
      throw new ConfigValueException('Body HTML is malformed');
    }
    return $this->set('body', $chunks);
  }

  /**
   * {@inheritdoc}
   */
  public function getBodyFormat() {
    return $this->get('body_format');
  }

  /**
   * {@inheritdoc}
   */
  public function setBodyFormat($format) {
    return $this->set('body_format', $format);
  }

  /**
   * {@inheritdoc}
   */
  public function isTopLevel() {
    return $this->get('top_level');
  }

  /**
   * {@inheritdoc}
   */
  public function setTopLevel($top_level) {
    return $this->set('top_level', $top_level);
  }

  /**
   * {@inheritdoc}
   */
  public function getRelated() {
    return $this->get('related');
  }

  /**
   * {@inheritdoc}
   */
  public function setRelated(array $topics) {
    return $this->set('related', $topics);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();

    if ($this->body_format) {
      $format = $this->entityTypeManager()->getStorage('filter_format')->load($this->body_format);
      if ($format) {
        $this->addDependency('config', $format->getConfigDependencyName());
      }
    }
    return $this;
  }

  /**
   * Gets the help topic plugin manager.
   *
   * @return \Drupal\help\HelpTopicPluginManagerInterface
   *   The help topic plugin manager.
   */
  protected function getPluginManager(): HelpTopicPluginManagerInterface {
    if (!isset($this->pluginManager)) {
      $this->pluginManager = \Drupal::service('plugin.manager.help_topic');
    }
    return $this->pluginManager;
  }

  /**
   * Finds the cache tag for a help topic plugin ID.
   *
   * @param string $id
   *   The plugin ID to find the cache tag of.
   *
   * @return string|null
   *   The main cache tag for the topic, or NULL if there is not one.
   */
  protected function pluginCacheTag($id) {
    $definition = $this->getPluginManager()->getDefinition($id);
    if (!$definition || !isset($definition['cache_tag'])) {
      return NULL;
    }
    return $definition['cache_tag'];
  }

  /**
   * Returns the cache tag for this entity.
   *
   * @return string
   *   The cache tag to use for this entity.
   */
  protected function makeConfigCacheTag() {
    return 'config:' . $this->getEntityType()->getConfigPrefix() . '.' . $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate() {
    // Get the standard bare cache tags.
    $tags = parent::getCacheTagsToInvalidate();

    // In addition to the standard entity tags, add the tags for plugins
    // this topic is related to, so that when we edit or delete this entity,
    // those others will also have their render caches invalidated.
    foreach ($this->related as $topic) {
      $tag = $this->pluginCacheTag($topic);
      if ($tag) {
        $tags[] = $tag;
      }
    }
    return $tags;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    // Get the standard bare entity cache tags.
    $tags = parent::getCacheTagsToInvalidate();

    // In addition to the standard entity tags, add the tags for topics
    // listed on this topic, so that if they are edited or deleted, this one
    // will have its render cache invalidated.
    foreach ($this->related as $topic) {
      $tag = $this->pluginCacheTag($topic);
      if ($tag) {
        $tags[] = $tag;
      }
    }

    // Also add the standard list cache tag, because the related stuff depends
    // on this, and the cache tag for help topics lists.
    $tags[] = 'config:config_help_list';
    $tags[] = 'help_topics';

    return $tags;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // Invalidate cache tags for topics this one is related to.
    if (!$this->isNew()) {
      $original = $storage->loadUnchanged($this->getOriginalId());
      $tags = [];
      foreach ($original->related as $topic) {
        $tag = $this->pluginCacheTag($topic);
        if ($tag) {
          $tags[] = $tag;
        }
      }
      if (count($tags)) {
        Cache::invalidateTags($tags);
      }
    }

    $this->getPluginManager()->clearCachedDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);
    \Drupal::service('plugin.manager.help_topic')->clearCachedDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinition() {
    $definition = [
      'metadata' => ['entity_id' => $this->id],
      'id' => $this->id,
      'label' => $this->label,
      'body' => $this->body,
      'body_format' => $this->body_format,
      'top_level' => $this->top_level,
      'related' => $this->related,
      'cache_tag' => $this->makeConfigCacheTag(),
    ];
    return $definition;
  }

  /**
   * {@inheritdoc}
   */
  public function getViewUrl(array $options = []) {
    return Url::fromRoute('help.help_topic', ['id' => 'config_help:' . $this->getPluginId()], $options);
  }

  /**
   * {@inheritdoc}
   */
  public function getViewLink($text = NULL, array $options = []) {
    if (!$text) {
      $text = $this->get('label');
    }
    return Link::createFromRoute($text, 'help.help_topic', ['id' => 'config_help:' . $this->getPluginId()], $options);
  }

  /**
   * Return TRUE for viewing.
   */
  public function isDefaultRevision($new_value = NULL) {
    return TRUE;
  }

}
