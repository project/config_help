Configurable Help module

This module provides a way for site administrators to create and edit help topic
pages. These pages are part of your site configuration, and can therefore be
translated, imported, and exported.

This module works in conjunction with the core (currently Experimental) Help
Topics module. You will only be able to edit the topics you create, not the
built-in topics that modules and themes provide using the Help Topics
module. However, all topics will be seamlessly displayed together.

INSTALLATION

Install the module in the normal way for Drupal modules. The only dependency is
the core Help and Help Topics modules.

If you install the core Configuration Translation module (and its dependencies,
Interface Translation and Language), and if you configure languages on your
site, you will also be able to translate your site's help topics.


TOPIC ADMINISTRATION

Visit Administration >> Configuration >> Development >> Help (path
admin/config/development/config-help) to see a list of the help topics on your
site, edit them, translate them (for multilingual sites), and add new topics.


VIEWING TOPICS

All topics where the "Top-level" field has value TRUE are listed on the main
Help page (path admin/help), along with topics provided by modules and
themes. Topics where "Top-level" is FALSE will not be listed on this page, but
they may be linked in the Related Topics section of other help topics.

Administrators can also view individual help topics by clicking on the topic
title from the help topic administration page.


SHARING TOPICS WITH OTHER SITES

If you have created help topics that you would like to share with other sites,
you can use the Configuration management pages to export and import them.

To export a topic, visit Administration >> Configuration >> Development >>
Configuration synchronization >> Export >> Single item (path
admin/config/development/configuration/single/export). Choose Configuration Type
"Help topic", and the title of your topic to export; save it in the suggested
file name.

To import a topic, visit Administration >> Configuration >> Development >>
Configuration synchronization >> Import >> Single item (path
admin/config/development/configuration/single/import). Choose Configuration Type
"Help topic", and paste the contents of a previously-exported configuration file
into the text box.
