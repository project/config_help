<?php

declare(strict_types=1);

namespace Drupal\Tests\config_help\Functional;

use Drupal\Component\Serialization\Json;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the administrative interface for help topic entities.
 *
 * @group help
 */
class HelpTopicAdminTest extends BrowserTestBase {

  /**
   * Modules to enable, including one for dependency testing (color).
   *
   * @var array
   */
  protected static $modules = [
    'filter',
    'block',
    'help',
    'config_help',
    'config_help_test',
    'dblog',
  ];

  /**
   * User who can administer help topics and view them.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Non-admin user to test access to admin pages is blocked.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $nonAdminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create users.
    $this->adminUser = $this->createUser([
      'access administration pages',
      'access help pages',
      'administer config help',
      'use text format help',
      'access site reports',
    ]);

    $this->nonAdminUser = $this->createUser(['access help pages']);

    // Make sure page title, help, and local tasks are showing.
    $this->placeBlock('local_tasks_block');
    $this->placeBlock('local_actions_block');
    $this->placeBlock('page_title_block');
    $this->placeBlock('help_block');
  }

  /**
   * Logs in users, tests help admin pages.
   */
  public function testHelpAdmin() {
    $this->drupalLogin($this->adminUser);
    $this->verifyHelpAdmin();

    $this->drupalLogin($this->nonAdminUser);
    $this->verifyHelpAdmin(403);
  }

  /**
   * Verifies the logged in user has the correct access to help admin.
   *
   * @param int $response
   *   (optional) The HTTP response code to test for. If it's 200 (default),
   *   the test verifies the user has access; if it's not, it verifies they
   *   are denied access.
   */
  protected function verifyHelpAdmin($response = 200) {
    // Verify admin links.
    foreach ([
      'admin/config',
      'admin/config/development',
      'admin/index',
    ] as $page) {
      $this->drupalGet($page);
      $session = $this->assertSession();
      if ($response == 200) {
        $session->pageTextContains('Add, delete, and edit help topics');
        $session->linkExists('Help topics');
      }
      else {
        $session->pageTextNotContains('Add, delete, and edit help topics');
        $session->linkNotExists('Help topics');
      }
    }

    // Verify CRUD and listing page.
    $this->drupalGet('admin/config/development/config-help');
    $session = $this->assertSession();
    $session->statusCodeEquals($response);
    if ($response == 200) {
      $session->linkExists('Add new help topic');
      $session->pageTextContains('Help topics');
      $session->pageTextContains('Title');
      $session->pageTextContains('Machine name');
      $session->pageTextContains('Operations');
    }

    $this->drupalGet('admin/config/development/config-help/add');
    $session = $this->assertSession();
    $session->statusCodeEquals($response);

    // Verify autocomplete page.
    $this->drupalGet('config-help/autocomplete-topic');
    $session = $this->assertSession();
    $session->statusCodeEquals($response);

    // Everything after this point, just do for the admin user.
    if ($response != 200) {
      return;
    }

    // Create a new help topic from the UI.
    $body = 'This text is for the foo topic';
    $title = 'Foo topic';
    $id = 'foo';
    $this->drupalGet('admin/config/development/config-help/add');
    $this->submitForm([
      'label' => $title,
      'id' => $id,
      'top_level' => TRUE,
      'body[value]' => $body,
    ], 'Save');
    $session = $this->assertSession();
    $session->pageTextContains("The help topic $title has been added.");

    // After creating, we should be on the View page.
    $session = $this->assertSession();
    $session->pageTextContains($title);
    $session->pageTextContains($body);

    // Edit the topic.
    $new_title = 'Foo longer topic';
    $new_id = 'foo2';
    $this->drupalGet('admin/config/development/config-help/manage/' . $id);
    $this->submitForm([
      'label' => $new_title,
      'id' => $new_id,
    ], 'Save');
    // After editing, we should be back on the View page.
    $session = $this->assertSession();
    $session->pageTextContains("The help topic $new_title has been updated.");
    $session->linkExists($new_title);
    $session->pageTextContains($body);

    // Test a few autocomplete values for topics we just created.
    $queries = [
      // ID of a topic we just added.
      $new_id => [$new_title, 'config_help:' . $new_id],
      // Title word of a topic we just edited.
      'longer' => [$new_title, 'config_help:' . $new_id],
      'help_test' => [
        'Additional topic',
        'config_help:help_test_additional',
        'Help Test module',
      ],
    ];

    foreach ($queries as $query => $texts) {
      $this->drupalGet('config-help/autocomplete-topic', ['query' => ['q' => $query]]);
      $session = $this->assertSession();
      $session->statusCodeEquals($response);
      foreach ($texts as $text) {
        $session->responseContains($text);
      }
    }

    // Verify the link is on the Help page.
    $this->drupalGet('admin/help');
    $session = $this->assertSession();
    $session->linkExists($new_title);

    // Test deleting.
    $this->drupalGet('admin/config/development/config-help/manage/' . $new_id . '/delete');
    $session = $this->assertSession();
    $session->pageTextContains('This action cannot be undone.');
    $session->pageTextContains('Are you sure you want to delete the configurable help topic');
    $session->pageTextContains($new_title);
    $this->submitForm([], 'Delete');
    $session = $this->assertSession();
    $session->pageTextContains("The configurable help topic $new_title has been deleted");

    // Verify we are back on the admin page and there is no longer a link
    // to the topic we just deleted.
    $session->linkExists('Add new help topic');
    $session = $this->assertSession();
    $session->pageTextContains('Help topics');
    $session->pageTextContains('Title');
    $session->pageTextContains('Machine name');
    $session->pageTextContains('Operations');
    $session->linkNotExists($new_title);
    // Verify the topic is not on admin/help any more either.
    $this->drupalGet('admin/help');
    $session = $this->assertSession();
    $session->linkNotExists($new_title);

    // Test form validation.
    $this->drupalGet('admin/config/development/config-help/add');
    $this->submitForm([
      'label' => $title,
      'id' => 'foobar',
      'top_level' => TRUE,
      'body[value]' => $body,
      'related' => 'invalid.text',
    ], 'Save');
    $session = $this->assertSession();
    $session->pageTextNotContains('has been added');
    $session->pageTextContains('Must be a comma-separated list of existing topic machine names');
  }

  /**
   * Tests tabs on topic view page.
   */
  public function testTabsVisible() {
    // Install config translation module to test translation tab.
    $this->container->get('module_installer')
      ->install(['config_translation', 'locale', 'language']);
    ConfigurableLanguage::createFromLangcode('es')->save();

    $user = $this->createUser([
      'access administration pages',
      'administer config help',
      'use text format help',
      'translate configuration',
    ]);
    $this->drupalLogin($user);
    foreach (['Edit', 'Delete', 'Translate configurable help topic'] as $label) {
      $this->drupalGet('admin/config/development/config-help/manage/help_test');
      $this->clickLink($label);
      $session = $this->assertSession();
      $session->statusCodeEquals(200);
    }
  }

  /**
   * Tests autocomplete for topics.
   */
  public function testAutocomplete() {
    $path = 'config-help/autocomplete-topic';
    $cases = [
      // Test case for a non-matching response.
      '|' => '[]',
      // Test case for matching on topic title.
      'plugin-based topic' => '[{"value":"config_help_test.foo","label":"Test plugin-based topic (config_help_test.foo)"}]',
      // Test case for matching on machine name.
      'test.foo' => '[{"value":"config_help_test.foo","label":"Test plugin-based topic (config_help_test.foo)"}]',
    ];
    // Test that non-admin user has no access to autocomplete routes.
    $this->drupalLogin($this->nonAdminUser);
    $this->drupalGet($path);
    $session = $this->assertSession();
    $session->statusCodeEquals(403);

    // Test output of autocomplete.
    $this->drupalLogin($this->adminUser);
    foreach ($cases as $query => $expected) {
      $this->drupalGet($path, ['query' => ['q' => $query]]);
      $session = $this->assertSession();
      $session->statusCodeEquals(200);
      $this->assertEquals($expected, $this->getSession()->getPage()->getContent());
    }
    // Make sure results are limited.
    $this->drupalGet($path, ['query' => ['q' => 'o']]);
    $matches = Json::decode($this->getSession()->getPage()->getContent());
    $this->assertEquals(10, count($matches));
  }

}
