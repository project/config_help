<?php

declare(strict_types=1);

namespace Drupal\Tests\config_help\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Verifies help topic display and user access to help based on permissions.
 *
 * @group help
 */
class HelpTopicTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'help',
    'config_help',
    'config_help_test',
    'block',
    'filter',
  ];

  /**
   * The admin user that will be created.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * The anonymous user that will be created.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $anyUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // These tests rely on some markup from the 'Seven' theme.
    \Drupal::service('theme_installer')->install(['claro']);
    \Drupal::service('config.factory')->getEditable('system.theme')->set('admin', 'claro')->save();

    // Place various blocks.
    $settings = [
      'theme' => 'claro',
      'region' => 'help',
    ];
    $this->placeBlock('help_block', $settings);
    $this->placeBlock('local_tasks_block', $settings);
    $this->placeBlock('local_actions_block', $settings);
    $this->placeBlock('page_title_block', $settings);

    // Create users.
    $this->adminUser = $this->createUser([
      'access help pages',
      'administer site configuration',
      'view the administration theme',
      'administer permissions',
      'administer config help',
    ]);
    $this->anyUser = $this->createUser([]);
  }

  /**
   * Tests the main help page and individual pages for topics.
   */
  public function testHelp() {
    // Log in the regular user.
    $this->drupalLogin($this->anyUser);
    $this->verifyHelp(403, FALSE);

    // Log in the admin user.
    $this->drupalLogin($this->adminUser);
    $this->verifyHelp();
    $this->verifyHelpLinks();

    // Verify that help topics text appears on admin/help.
    $this->drupalGet('admin/help');
    $session = $this->assertSession();

    // Verify the cache tag for the list of topics is present, as well as
    // the cache context for user permissions.
    $this->assertCacheTag('config:config_help_list');
    $this->assertCacheContext('user.permissions');

    // Verify links for for configurable topics, and order.
    $page_text = $this->getTextContent();
    $pos = 0;
    foreach ($this->getTopicList() as $info) {
      $name = $info['name'];
      $session->linkExists($name);
      $new_pos = strpos($page_text, $name);
      $this->assertTrue($new_pos > $pos, 'Order of ' . $name . ' is correct on page');
      $pos = $new_pos;
    }
  }

  /**
   * Verifies the logged in user has access to various help links and pages.
   *
   * @param int $response
   *   (optional) The HTTP response code to test for. If it's 200 (default),
   *   the test verifies the user sees the help; if it's not, it verifies they
   *   are denied access.
   * @param bool $check_tags
   *   (optional) TRUE (default) to verify that the cache tags are on the page,
   *   even though the response is not a 200. Cache tags should be there except
   *   for users who don't even have 'view help topics' permission.
   */
  protected function verifyHelp($response = 200, $check_tags = TRUE) {
    // Verify access to configurable help topic pages.
    foreach ($this->getTopicList() as $topic => $info) {
      // View help topic page.
      $this->drupalGet('admin/help/topic/' . $topic);
      $session = $this->assertSession();
      $session->statusCodeEquals($response);
      if ($response == 200) {
        $name = $info['name'];
        $session->titleEquals($name . ' | Drupal');
        $session->responseContains('<h1 class="page-title">' . $name . '</h1>');
        // Check the cache tags.
        foreach ($info['cache_tags'] as $tag) {
          $this->assertCacheTag($tag);
        }
      }
      elseif ($check_tags) {
        // Check that the cache tags are there.
        foreach ($info['cache_tags'] as $tag) {
          $this->assertCacheTag($tag);
        }
      }
    }
  }

  /**
   * Verifies links on the test help topic page and other pages.
   *
   * Assumes an admin user is logged in.
   */
  protected function verifyHelpLinks() {
    // Verify links on the test top-level page.
    $page = 'admin/help/topic/config_help:help_test';
    $links = [
      'link to the Help module topic' => 'Building a help system',
      'link:to the help admin page' => 'Add new help topic',
      'Linked topic' => 'This topic is not supposed to be top-level',
      'Test plugin-based topic' => 'Provide a topic for testing',
    ];
    foreach ($links as $link_text => $page_text) {
      $this->drupalGet($page);
      $this->clickLink($link_text);
      $session = $this->assertSession();
      $session->pageTextContains($page_text);
    }

    // Verify that the non-top-level topics do not appear on the Help page.
    $this->drupalGet('admin/help');
    $session = $this->assertSession();
    $session->linkNotExists('Linked topic');
    $session->linkNotExists('Additional topic');
  }

  /**
   * Gets a list of topic IDs to test.
   *
   * @return array
   *   A list of topics to test, in the order in which they should appear. The
   *   keys are the machine names of the topics. The values are arrays with the
   *   following elements:
   *   - name: Displayed name.
   *   - cache_tags: Cache tags to verify are present on the topic display page.
   */
  protected function getTopicList() {
    return [
      'config_help:help_test' => [
        'name' => 'ABC Help Test module',
        'cache_tags' => [
          'config:config_help.topic.help_test',
          'config:config_help.topic.help_test_linked',
          'config:filter.format.help',
        ],
      ],
      'config_help_test.foo' => [
        'name' => 'Test plugin-based topic',
        'cache_tags' => [
          'config:config_help.topic.help_test_additional',
        ],
      ],
    ];
  }

  /**
   * Asserts that a given cache context exists.
   *
   * This was part of the Simpletest base class, but doesn't exist in
   * BrowserTestBase.
   */
  protected function assertCacheContext($expected_cache_context) {
    $contexts = explode(' ', $this->getSession()->getResponseHeader('X-Drupal-Cache-Contexts'));
    $this->assertTrue(in_array($expected_cache_context, $contexts), "'" . $expected_cache_context . "' is present in the X-Drupal-Cache-Contexts header.");
  }

  /**
   * Asserts that a given cache tag exists.
   *
   * This was part of the Simpletest base class, but doesn't exist in
   * BrowserTestBase except in a legacy trait.
   */
  protected function assertCacheTag($expected_cache_tag) {
    $tags = explode(' ', $this->getSession()->getResponseHeader('X-Drupal-Cache-Tags'));
    $this->assertTrue(in_array($expected_cache_tag, $tags), "'" . $expected_cache_tag . "' is present in the X-Drupal-Cache-Tags header.");
  }

}
